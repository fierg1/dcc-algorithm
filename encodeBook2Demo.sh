#!/bin/bash

echo "executing  java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f <files> "

sleep 1

java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/f1.avif
java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/f1.jpg
java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/f1.webp
java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/video.gif
java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/video-001.gif
java -jar twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f data/corpus/pictures/video-005.gray.gif
