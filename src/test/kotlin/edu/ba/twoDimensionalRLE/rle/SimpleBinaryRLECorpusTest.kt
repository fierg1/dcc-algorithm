package edu.ba.twoDimensionalRLE.rle


import de.jupf.staticlog.Log
import edu.ba.twoDimensionalRLE.analysis.Analyzer
import edu.ba.twoDimensionalRLE.encoder.rle.StringRunLengthEncoder
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.io.File

@ExperimentalStdlibApi
@ExperimentalUnsignedTypes
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class SimpleBinaryRLECorpusTest {

    private var log = Log.kotlinInstance()
    private val strRLE = StringRunLengthEncoder()
    private val analyzer = Analyzer()

    init {
        log.newFormat {
            line(date("yyyy-MM-dd HH:mm:ss"), space, level, text("/"), tag, space(2), message, space(2))
        }
    }

    companion object {
        private const val folderToEncode = "data/corpus/CantrburyCorpus"
        private const val encodeFolder = "data/encoded/simple_rle"
        private const val decodeFolder = "data/decoded/simple_rle"
    }


    @Test
    @Order(1)
    fun cleanup() {
        if (File("$encodeFolder/CantrburyCorpus").exists()) {
            log.info("deleting directory: $encodeFolder/CantrburyCorpus")
            File("$encodeFolder/CantrburyCorpus").deleteRecursively()
            File("$decodeFolder/CantrburyCorpus").deleteRecursively()


        }
        log.info("creating directory: $encodeFolder/CantrburyCorpus")
        File("$encodeFolder/CantrburyCorpus").mkdirs()
        File("$decodeFolder/CantrburyCorpus").mkdirs()

    }

    @Test
    @Order(2)
    fun encodeSimpleRLE() {
        for (i in 2..8) {
            File("$encodeFolder/CantrburyCorpus/${i}bit").mkdirs()
            File(folderToEncode).listFiles().forEach {
                strRLE.encodeSimpleBinaryRLE(
                    "${folderToEncode}/${it.name}",
                    "${encodeFolder}/CantrburyCorpus/${i}bit/${it.name}",
                    i
                )
            }
            analyzer.sizeCompare(folderToEncode, "${encodeFolder}/CantrburyCorpus/${i}bit")
        }

    }
}