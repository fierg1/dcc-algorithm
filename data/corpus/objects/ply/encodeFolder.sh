for file in *.ply
do
  echo "$file"
  java -Xmx16G -jar ~/git/dcc-algorithm/target/twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f "$file"
  zip "$file.zip" "$file"
done

for file in *.ply.zip
do
  echo "$file"
  java -Xmx16G -jar ~/git/dcc-algorithm/target/twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f "$file"
done