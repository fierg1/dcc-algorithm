for file in *.ppm
do
  echo "$file"
  java -Xmx16G -jar ~/git/dcc-algorithm/target/twoDimensionalRLE-1.0.jar -v -huf -map -bwt -D -f "$file"
done
  