#!/bin/bash

java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/dickens
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/mozilla
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/mr
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/nci
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/ooffice
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/osdb
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/reymont
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/samba
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/sao
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/webster
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/xml
java -jar target/twoDimensionalRLE-1.0-SNAPSHOT-jar-with-dependencies.jar -v -huf -map -bwt -D -f data/corpus/Silesia/x-ray